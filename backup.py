#!/usr/bin/env python
# Script to backup miltiple websites and related DB's - Updated for Python
# Copyright (C) 2013 Alex Gonzalez - All Rights Reserved
# Permission to copy and modify is granted under the MIT license
# Last revised 03/04/13

import os
import hashlib
import subprocess
import tarfile
import tempfile
import ftplib
from datetime import date


class Site2BkUp():

    def __init__(self, Name, FileLoc, UseSQL, MySQLDB, MySQLUsr, MysqlPass, FTPIP, FTPPort, FTPUser, FTPPass, FTPDir):
        self.Name = Name
        self.FileLoc = FileLoc
        self.UseSQL = UseSQL
        self.MySQLDB = MySQLDB
        self.MySQLUsr = MySQLUsr
        self.MysqlPass = MysqlPass

        self.FTPIP = FTPIP
        self.FTPPort = FTPPort
        self.FTPUser = FTPUser
        self.FTPPass = FTPPass
        self.FTPDir = FTPDir

        self.LocalDir = os.getcwd()
        self.WorkingDir = tempfile.mkdtemp() + "/"
        self.Date = date.today()

        self.SiteAppWad = self.Name + "-App-" + \
            self.Date.isoformat() + ".tar.gz"

        self.SiteSQLWad = self.Name + "-Mysql-" + \
            self.Date.isoformat() + ".tar.gz"

        self.SiteMD5File = self.Name + "-MD5-" + \
            self.Date.isoformat() + ".txt"

        self.FileList = []

    def  __exit__(self, type, value, traceback):
        os.removedirs(self.WorkingDir)

    def SiteFileBackup(self):
        tar = tarfile.open(self.WorkingDir + self.SiteAppWad, "w:gz")
        tar.add(self.FileLoc, arcname=self.Name)
        tar.close()
        self.FileList.append(self.SiteAppWad)

    def SiteSQLBackup(self):
        cmdL1 = ["mysqldump", "--user=" + self.MySQLUsr,
                 "--password=" + self.MysqlPass, self.MySQLDB]
        p1 = subprocess.Popen(
            cmdL1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        dump_output = p1.communicate()[0]

        with tarfile.open(self.WorkingDir + self.SiteSQLWad, "w:gz") as tar:
            tar.add(dump_output)

        self.FileList.append(str(self.SiteSQLWad))

    def __MD5er(self, file):
        md5 = hashlib.md5()
        with open(file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b''):
                md5.update(chunk)
        return md5.hexdigest()

    def MD5List(self):
        with open(self.WorkingDir + self.SiteMD5File, 'w+') as f:
            for file in self.FileList:
                f.write(str(self.SiteAppWad + ": " + self.__MD5er(self.WorkingDir + file)))
        self.FileList.append(str(self.SiteMD5File))

    def SiteTransfer(self):
        try:
            with ftplib.FTP() as ftp:
                ftp.set_debuglevel(1)
                ftp.connect(host=self.FTPIP, port=self.FTPPort)
                ftp.login(user=self.FTPUser, passwd=self.FTPPass)
                ftp.set_pasv(False)
                ftp.cwd(self.FTPDir + "/")
                for item in self.FileList:
                    ftp.storlines('STOR ' + str(item), self.WorkingDir + item)
        except Exception as e:
            print(str(e))

    def RunBackup(self):
        self.SiteFileBackup()
        if self.UseSQL is True:
            self.SiteSQLBackup
        self.MD5List()
        self.SiteTransfer()


def main():

    ListOfSites = ["TheWad", "/home/ci/Code/testwad", False, "sqldb",
                   "sqlusr", "sqlpass"]

    FTPInfo = [,,,,]

    BigWad = ListOfSites + FTPInfo
    SiteWad = Site2BkUp(*BigWad)
    SiteWad.RunBackup()

if __name__ == '__main__':
    main()
