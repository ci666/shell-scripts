#My Personal Bash Scripts

This is my repository of shitty scripts I write day to day

Here are a few:

* [mod-backup.sh](https://bitbucket.org/ci666/shell-scripts/wiki/mod-backup.sh) - This backups script was designed to backup multiple websites and their databases and upload them to an FTP server. 
* [looptest.sh](https://bitbucket.org/ci666/shell-scripts/wiki/looptest.sh) - This script is a test of bash's multiprocessing abilitys. This method may be rolled into the mod-backup.sh script.
* [One Liners of Win](https://bitbucket.org/ci666/shell-scripts/wiki/One%20Liners%20of%20Win) - My Personal list of awesome one liners