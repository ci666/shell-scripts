#!/usr/bin/env bash
# Script to backup miltiple websites and related DB's
# Copyright (C) 2013 Alex Gonzalez - All Rights Reserved
# Permission to copy and modify is granted under the MIT license
# Last revised 1/29/13

#-- Variables --
AppList=()

#Enter as AppName AppPrefix AppLoc SqlPrefix SqlDB SqlUsr SqlPass
AppList+=(,,,,,,)

FTPIP=''
FTPPORT=''
FTPUSER=''
FTPPASS=''
FTPLOC=''

AdminEmail=

#-- Do not Edit --
REQPROGS='ftp awk tar date pv gzip pwd mysqldump md5sum'
CURLOC=$(pwd)
DATE=$(date +"%m%d%Y")
FILEPILE=()
APPNUM=${#AppList[@]}
CNUM=$(grep "^core id" /proc/cpuinfo | sort -u | wc -l)
MOD=$(($APPNUM % $CNUM))


#-- Functions --
EnviromentCheck () {
    echo -n 'Enviroment Check... '
    for i in $REQPROGS; do
        hash $i 2>/dev/null || { echo ''; echo >&2 "I require $1 but it's not installed.  Aborting."; exit 1; };
    done
    echo 'Passed'
}

AppPkg () {
#Called ATNAME AppLoc
    echo 'Packaging Webapp Files... '
    echo 'Creating' $1.tgz'... '
    cd $2
    tar -cf - . | pv -s $(du -sb . | awk '{print $1}') | gzip > /tmp/$1.tgz
    FILEPILE+=($1.tgz)
}

DBPkg () {
#Called STNAME SqlUser SqlPass SqlDBName
    echo 'Packaging Webapp DB'
    echo 'Creating' $1.tgz'...'
    cd /tmp/
    mysqldump -u$2 -p$3 $4 | pv -tb | gzip > /tmp/$1.sql.gz
    FILEPILE+=($1.sql.gz)
}

MD5Pkgs () {
#Called ATNAME STNAME
    echo -n 'MD5ing the Archives... '
    md5sum $1.tgz > $1.md5
    FILEPILE+=($1.md5)
    md5sum $2.sql.gz > $2.md5
    FILEPILE+=($2.md5)
    echo 'Done'
}

BuildWad () {
#Called TTNAME
    echo 'Creating ' $1 '... '
    tar -cf - ${FILEPILE[@]} | pv -s $(du -sb ${FILEPILE[@]} |  awk '{print SUM += $1}' | tail -n1) > $1
}

Upload2FTP () {
#Called TTNAME
    echo 'Sending' $1 'to server...'
    ftp -inv << EOF
        open $FTPIP $FTPPORT
        user $FTPUSER $FTPPASS
        passive
        put $1 $FTPLOC/$1
        bye
EOF
    echo 'Send Complete'
}

Cleanup () {
#Called ATNAME STNAME TTNAME LOGNAME
    echo -n 'Cleaning up... '
    rm $1.*
    rm $2.*
    rm $3
    rm $4
    cd $CURLOC
    echo 'Done'
}

MainLoop () {
# Called AppName AppPrefix AppLocation SqlPrefix SqlDBName SqlUser SqlPass

    local ATNAME=$1-$2-$DATE
    local STNAME=$1-$4-$DATE
    local TTNAME=$1-$DATE.tar
    local LOGNAME=$1.log

    exec > >(tee /tmp/$LOGNAME)
    exec 2>&1

    AppPkg $ATNAME $3 
    DBPkg $STNAME $6 $7 $5
    MD5Pkgs $ATNAME $STNAME
    BuildWad $TTNAME
    Upload2FTP $TTNAME

    mail -s "[Backup] - $(date -R) - $(hostname):$1" $AdminEmail < /tmp/$LOGNAME

    Cleanup $ATNAME $STNAME $TTNAME $LOGNAME
}

Main () {
echo -= WebApp Backup Script =-
echo

EnviromentCheck

for (( i=1; i<=$APPNUM; i++ ));
do
    (MainLoop $(echo ${AppList[$(($i-1))]} | sed 's/,/ /g')) &;
    if [ $(( $i % $CNUM )) -eq 0 ]; then wait; fi
done
if [ $MOD -ne 0 ]; then wait; fi
}

Main
