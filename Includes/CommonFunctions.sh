# EnvironmentCheck
#
# Checks if requires apps are avail
#
# Required Apps:
# 	None
#
# Required Arguments:
# 	'app1 app2 app3'
#
EnvironmentCheck () { 
    echo -n 'Environment Check... '
    local i=''
    for i in "$@"; do
        hash $i 2>/dev/null || { echo ''; echo >&2 "I require $1 but it's not installed.\t\tAborting."; exit 1; };
    done
    echo 'Passed'
}

# CoreNum
#
# Returns number for 'cores'
#
# Required Apps:
#       None
#
# Required Variables: 
#       None
#
CoreNum () {
	grep "^core id" /proc/cpuinfo | sort -u | wc -l
}

# Fork2Num
#
# Run a specific number of jobs in the background at a time. 
#
# Required Apps:
#       None
#
# Required Variables:
#       $1=Number of forks to run at once 
# 	$2=Array of jobs
#
Fork2Num () {
	
	declare -a Tasks=("${!2}")
	local i=''

	for (( i=1; i<=${#Tasks[@]}; i++ ))
	do
		eval ${Tasks[($i-1)]} & #Task to fork
		if [ $(( $i % $1 )) -eq 0 ]; then wait; fi
	done
	if [ $((${#Tasks[@]} % $1)) -ne 0 ]; then wait; fi
}

# TargzDir
#
# TarGZ's a dir and provides a handy progess bar
#
# Required Apps:
#       pv
#	gzip
#
# Required Variables:
#       $1=Dir containing stuff to tar
#       $2=output file name
#
TargzDir () {
    local prvdir=$(pwd)
    cd $1
    tar -cf - . | pv -s $(du -sb . | awk '{print $1}') | gzip > $2
    cd $prvdir
}

