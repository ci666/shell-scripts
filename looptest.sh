#!/usr/bin/env bash
# Script to test multithreading in Bash
# Copyright (C) 2013 Alex Gonzalez - All Rights Reserved
# Permission to copy and modify is granted under the MIT license
# Last revised 1/29/13

TASKS=()
TASK+=(ham0)
TASK+=(ham1)
TASK+=(ham2)
TASK+=(ham3)
TASK+=(ham4)
TASK+=(ham5)
TASK+=(ham6)
TASK+=(ham7)
TASK+=(ham8)
TASK+=(ham9)
TASK+=(ham10)

TNUM=${#TASK[@]}
CNUM=$(grep "^core id" /proc/cpuinfo | sort -u | wc -l)
MOD=$(($TNUM % $CNUM))
DELAY=2

# == Start ==
echo -=Loop Test=-
echo 
echo Starting Run
echo --------------
echo Cores: $CNUM 
echo Tasks: $TNUM
echo Delay: $DELAY
echo Mod: $MOD
echo --------------


for (( i=1; i<=$TNUM; i++ ))
do
	(sleep $DELAY; echo Task: $i - ${TASK[$(($i-1))]}) & #Task to fork

	if [ $(( $i % $CNUM )) -eq 0 ]; then wait; echo Waiting...; fi #Pauses forking once core limit is reached
done
if [ $MOD -ne 0 ]; then wait; echo Waiting...; fi #Waits for final tasks to finish

echo Tasks Complete.
