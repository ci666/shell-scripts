#!/usr/bin/env bash
# Script to generate easy to remember names for servers
# Copyright (C) 2013 Alex Gonzalez - All Rights Reserved
# Permission to copy and modify is granted under the MIT license
# Last revised 2/5/13


AdjuctivesFile=./Data/adjectives
AnimalNameFile=./Data/animalnames

Name1st=$(cat $AdjuctivesFile | sort -R | head -n 1)
Lastname=$(cat $AnimalNameFile | grep ^$(echo $Name1st | awk '{print substr($0,0,1)}') | sort -R | head -n1)


echo $Name1st $Lastname
